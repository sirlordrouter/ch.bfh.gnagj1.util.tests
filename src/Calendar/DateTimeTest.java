package Calendar;

import static org.junit.Assert.*;

import java.util.GregorianCalendar;

import junit.framework.Assert;

import org.junit.Test;

public class DateTimeTest {

	private String aTestDate = "12.04.2013";
	private String aTestDateTime = "12.04.2013 13:30:00";
	
	@Test
	public void testGetFormattedDateTimeString() {
		
		String result = DateTime.getFormattedDateString(new GregorianCalendar(2013, 04, 12));
		Assert.assertEquals(aTestDate, result);
	}

	@Test
	public void testParseDateString() {
		//fail("Not yet implemented");
	}

	@Test
	public void testParseDateTimeString() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetFormattedDateTimeStringFromCalendar() {
		String result = DateTime.getFormattedDateTimeString(new GregorianCalendar(2013, 04, 12, 13,30,0));
		Assert.assertEquals(aTestDateTime, result);
	}

}
